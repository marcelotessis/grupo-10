
import copa.Pessoa;

public class Torcedor extends Pessoa {
 
	private String selecao, cidadedeorigem, listadejogos;
	 
	public void setSelecao(String selecao) { this.selecao = selecao; }
        public String getSelecao() { return this.selecao; }
        
        public void setCidadedeorigem(String cidadedeorigem) { this.cidadedeorigem = cidadedeorigem; }
        public String getCidadedeorigem() { return this.cidadedeorigem; }
        
        public void setListadejogos(String listadejogos) { this.listadejogos = listadejogos; }
        public String getListadejogos() { return this.listadejogos; }
}