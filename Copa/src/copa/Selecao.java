/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package copa;

/**
 *
 * @author Marcelo Tessis
 */
public class Selecao {
    private String pais;
    private String continente;
    private String tecnico;
    private String jogadores;
    private int vitorias;
    private int empates;
    private int derrotas;
    private int amarelos;
    private int vermelhos;
    
    public Selecao(String pais,String continente,String tecnico, String jogadores, int vitorias, int empates,int derrotas, int amarelos, int vermelhos){
    this.pais = pais;
    this.continente = continente;
    this.tecnico = tecnico;
    this.jogadores = jogadores;
    this.vitorias = vitorias;
    this.empates = empates;
    this.derrotas = derrotas;
    this.amarelos = amarelos;
    this.vermelhos = vermelhos;
    
    }
    
    public String getPais(){
    return pais;
    }
    public String getContinente(){
    return continente;
    }
    public String getTecnico(){
    return tecnico;
    }
    public String getJogadores(){
    return jogadores;
    }
    public int getVitorias(){
    return vitorias;
    }
    public int getEmpates(){
    return empates;
    }
    public int getDerrotas(){
    return derrotas;
    }
    public int getAmarelos(){
    return amarelos;
    }
    public int getVermelhos(){
    return vermelhos;
    }
    public void setPais(String p){
    pais = p;
    }
    public void setContinente(String c){
    continente = c;
    }
    public void setTecnico(String t){
    tecnico = t;
    }
    public void setJogadores(String j){
    jogadores = j;
    }
    public void setVitorias(int v){
    vitorias = v;
    }
    public void setEmpates(int e){
    empates = e;
    }
    public void setDerrotas(int d){
    derrotas = d;
    }
    public void setAmarelos(int a){
    amarelos = a;
    }
    public void setVermelhos(int v){
    vermelhos = v;
    }
    
    
    
    
    
}
