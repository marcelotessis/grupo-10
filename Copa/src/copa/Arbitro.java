
import copa.Pessoa;

public class Arbitro extends Pessoa {
 
	private String tipo, nrocartoesamarelos, nrocartoesvermelhos, nrofaltas;
        	 
	public void setTipo(String tipo) { this.tipo = tipo; }
        public String getTipo() { return this.tipo; }
        
        public void setNrocartoesamarelos(String nrocartoesamaralos) { this.nrocartoesamarelos = nrocartoesamarelos; }
        public String getNrocartoesamarelos() { return this.nrocartoesamarelos; }
        
        public void setNrocartoesvermelhos(String nrocartoesvermelhos) { this.nrocartoesvermelhos = nrocartoesvermelhos; }
        public String getNrocartoesvermelhos() { return this.nrocartoesvermelhos; }
	 
	public void setNrofaltas(String nrofaltas) { this.nrofaltas = nrofaltas; }
        public String getNroaltas() { return this.nrofaltas; }
}